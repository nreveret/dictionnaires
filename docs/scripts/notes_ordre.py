def notes(les_notes) -> list:
    liste_notes = [les_notes[devoir] for devoir in les_notes]
    return liste_notes


print({"test_1": 14, "test_2": 16, "test_3": 18} == {"test_3": 18, "test_2": 16, "test_1": 14})
print(notes({"test_1": 14, "test_2": 16, "test_3": 18}))
print(notes({"test_3": 18, "test_2": 16, "test_1": 14}))
