def nombre_animaux(animal, ferme):
    if ... ... ...:
        ...
    ...


ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}

# Tests
assert nombre_animaux("lapin", ferme_gaston) == 5, "Il y a 5 lapins"
assert nombre_animaux("tyrannausore", ferme_gaston) == 0, "Il y a 0 tyrannausore !"
