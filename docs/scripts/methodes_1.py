notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}

print("Parcours avec la méthode keys :")
for devoir in notes_Alice.keys():
    print(devoir)

print("Parcours avec la méthode values :")
for note in notes_Alice.values():
    print(note)

print("Parcours avec la méthode items :")
for (devoir, note) in notes_Alice.items():
    print(devoir, note)
