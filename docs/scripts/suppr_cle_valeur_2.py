ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}

valeur_cochon = ferme_gaston.pop("cochon")  # pop renvoie la valeur associée

# Affichage (pour vérifier)
print(ferme_gaston)
print("Il y avait", valeur_cochon, "cochons.")
