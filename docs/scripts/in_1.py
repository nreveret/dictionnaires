ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4, "poule": 0}

print("Gaston possède-t-il un lapin ?", "lapin" in ferme_gaston)
print("Gaston possède-t-il une poule ?", "poule" in ferme_gaston)
print("Gaston possède-t-il un lion ?", "lion" in ferme_gaston)
print("Gaston ne possède-t-il pas de lapin ?", "lapin" not in ferme_gaston)
print("Gaston ne possède-t-il pas de castor ?", "castor"  not in ferme_gaston)
