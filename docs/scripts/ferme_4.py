# Les deux dictionnaires
ferme_gaston = {"lapin": 11, "vache": 7, "cochon": 2, "cheval": 4}
ferme_gaston_secours = {"cochon": 2, "lapin": 11, "cheval": 4, "vache": 7}

# Affichages
print("ferme_gaston = ", ferme_gaston)
print("ferme_gaston_secours = ", ferme_gaston_secours)

# Les dictionnaires sont-ils égaux ?
print("ferme_gaston == ferme_gaston_secours ->", ferme_gaston == ferme_gaston_secours)
