def courses(produits, quantites):
    panier = {}
    ...


# Tests
assert courses(["farine", "beurre", "oeufs"], [1, 2, 12]) == {"farine": 1, "beurre": 2, "oeufs": 12}
assert courses(["pain", "confiture"], [2, 1]) == {"pain": 2, "confiture": 1}
