from collections import namedtuple  # Pour utiliser des tuples nommés

# on crée une structure de données "Personne" et l'on indique les descripteurs
Personne = namedtuple("Personne", ["nom", "prenom", "annee_naissance", "mel", "ville"])

alice = Personne("Martin", "Alice", 2003, "alice@mon_mail.fr", "Melun")
bob = Personne("Dupond", "Bob", 2004, "bob@mon_mail.fr", "Le Mée")
gaston = Personne("Durand", "Gaston", 2000, "gaston@mon_mail.fr", "Savigny")

print(alice.nom)
print(bob.mel)
print(gaston.ville)
