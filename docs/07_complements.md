---
author: Mireille Coilhac, Nicolas Revéret
title: Compléments
---

## p-uplets nommés

Prenons l'exemple suivant : 

```python
alice = ("Martin", "Alice", 2003, "alice@mon_mail.fr", "Melun")
bob = ("Dupond", "Bob", 2004, "bob@mon_mail.fr", "Le Mée")
gaston = ("Durand", "Gaston", 2000, "gaston@mon_mail.fr", "Savigny")
```

Ces tuples ont tous la même structure : `#!py (nom, prénom, anné, mél., ville)`. Si l'on désire l'année de naissance d' Alice, il faut se souvenir qu'on y accède avec `#!py alice[2]`. Pour l'adresse mél., on fait `#!py alice[3]`.

Les p-uplets nommés de `Python` permettent de remplacer les indices d'un p-uplet par des clés pour accéder à un élément. Dans le cas présent :

* l'indice `#!py 0` correspond à la clé `#!py "nom"`,
* l'indice `#!py 1` à `#!py "prenom"`,
* l'indice `#!py 2` à `#!py "annee_naissance"`,
* l'indice `#!py 3` à `#!py "mail"`,
* l'indice `#!py 4` à `#!py "ville"`.

Observons le fonctionnement en `Python` :

{{IDE('../scripts/puplets_nommes')}}

Contrairement aux dictionnaires, les p-uplets nommés sont **immuables**.

Un de leur intérêt est de permettre d'accéder aux éléments d'un p-uplet grâce à une clé, plutôt qu'avec un indice.

## Les tables associatives en quelques mots

Les dictionnaires `Python` sont mis en œuvre à l'aide de *tables associatives*. L'idée en est la suivante :

* lorsque l'on crée un dictionnaire, `Python` crée en mémoire un tableau contenant un certain nombre de cellules (8 par défaut),

* lors de l'ajout d'un couple `(clé: valeur)`, `Python` calcule l'indice d'une cellule à partir de la valeur de la clé. Il utilise pour cela une fonction de hachage.

* l'indice étant calculé, il est possible de trouver facilement la valeur associée en accédant à la cellule du tableau.

![Une table associative](hash.svg){ .center width=50% .autolight}

Source : [Jorge Stolfi - Own work, CC BY-SA 3.0](https://commons.wikimedia.org/w/index.php?curid=6471238)

Il reste toutefois plusieurs points à préciser :

* que faire si deux clés pointent vers la même cellule du tableau ?

* quelle fonction de hachage choisir ?

* comment redimensionner un dictionnaire ?

Les plus curieux (et téméraires !) pourront lire le [code source de `Cpython`](https://github.com/python/cpython/blob/main/Objects/dictobject.c).
