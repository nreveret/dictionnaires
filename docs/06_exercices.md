---
author: Charles Poulmaire, Mireille Coilhac, Nicolas Revéret et l'équipe e-nsi
title: Exercices
---

# 👷🏽 Exercices

??? question "Gaston ne se souvient plus ..."

    Gaston cherche à retrouver le nombre de poules mais il a fait une erreur.

    Corrigez-là.

    {{ IDE('../scripts/poules') }}

    ??? success "Solution"

??? question "Gaston achète des animaux..."

    <iframe src="https://www.codepuzzle.io/IPTFNH" width="100%" height="600" frameborder="0"></iframe>

??? question "Gaston vend des animaux"

    Voici la ferme de Gaston : `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}`

    Il a décidé de vendre un animal de chaque espèce. On garantit qu'il possède au moins un animal de chaque espèce.

    Compléter la fonction suivante, qui diminue chaque valeur de `#!py 1` dans le dictionnaire. On modifiera directement le dictionnaire (on dit que la modification est « en place »).

    {{IDE('../scripts/vend_1')}}

    ??? success "Solution"

        ```python
        def vend(ferme):
            for cle in ferme:
                ferme[cle] = ferme[cle] - 1
        ```

??? question "Gaston vend (encore) des animaux"

    Voici la ferme de Gaston : `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}`

    Il a décidé de vendre un animal de chaque espèce. On garantit qu'il possède au moins un animal de chaque espèce.

    Compléter la fonction suivante, qui diminue chaque valeur de `#!py 1` dans le dictionnaire. On modifiera directement le dictionnaire (on dit que la modification est « en place »).

    La solution de l'exercice précédent ne lui convient pas. En effet, un animal qui a un effectif de `#!py 0` ne doit pas se trouver dans le dictionnaire.

    Compléter la fonction [sur cette page](https://console.basthon.fr/?script=eJxLSU1TKEvNS9FISy3KTdW04uVSAAI9PT1eLl4usFh8emJxSX6egq1CtVJOYkFmnpKVgqmOglJZYnJGKpBtDmQn5ydn5IMkDEGcjNSyxBwgx6SWlwthNtQcTZDBygohqcUlxbxcicXFqUUlCqgWIdtkgmSTGbLhxrUggwDXVDdd) qui remédie à ce problème.

    ??? tip "Aide"

        `Python` interdit de modifier un dictionnaire lors de son parcours...

        Vous pouvez utiliser une liste Python `a_supprimer`.

    ??? success "Solution"

        ```python
        def vend(ferme):
            a_supprimer = []
            for cle in ferme:
                ferme[cle] = ferme[cle] - 1
                if ferme[cle] == 0:
                    a_supprimer.append(cle)
            for cle in a_supprimer :
                del ferme[cle]
        ```


??? question "Gaston fait ses courses"

    Gaston a décidé de préparer sa liste de courses qu'il va commander, avant d'aller la chercher au *DRIVE* de son supermarché. Quel va être le montant de sa facture ?

    Écrire une fonction `prix_total` qui prend en paramètres : 

    * `commande`, un dictionnaire contenant comme clés les produits souhaités par Gaston et comme valeurs associées la quantité désirée de chacun d'entre eux,
    * `catalogue` un dictionnaire contenant tous les produits du magasin avec leur prix associé.

    La fonction renvoie le montant total des achats de Gaston.

    On garantit que les produits listés par Gaston figurent bien dans le catalogue du magasin.

    ```pycon
    >>> commande = {"brocoli": 2, "mouchoirs": 5, "bouteilles d'eau": 6}
    >>> catalogue = {"brocoli": 1.5, "bouteilles d'eau": 1, "bière": 2, "savon": 2.5, "mouchoirs": 0.8}
    >>> prix_total(commande, catalogue)
    13.0
    ```

    {{IDE('../scripts/achats_2')}}

    ??? success "Solution"

        ```python
        def prix_total(produits, catalogue):
            prix = 0
            for produit in commande:  # On aurait aussi pu écrire : for cle in produits.keys()
                prix = prix + commande[produit] * catalogue[produit]
            return prix
        ```

        Mathématiquement parlant, le résultat du second test est $63,8$. Or `Python` affiche `#!py 63.79999999999999`.

        Le problème provient de l'encodage des nombres flottants et c'est pourquoi on ne teste pas l'égalité des flottants mais plutôt leur proximité : `#!py assert prix_total(commande, catalogue) - 63.8 < 1e-3`.


??? question "Occurrences des caractères"

    Quelles sont les lettres nécessaires pour écrire le mot "brontosaurus" ? Il faut une lettre "b", deux lettres "r", deux lettres "o", *etc*.

    [Dictionnaire d'occurrences](https://codex.forge.apps.education.fr/exercices/dico_occurrences/){ .md-button target="_blank" rel="noopener" }

??? question "Top 3"

    Un jury doit attribuer le prix du « Codeur de l'année ».

    Afin de récompenser les trois candidats ayant obtenu la meilleure note, il souhaite écrire une fonction `top_3` qui reçoit en paramètre un dictionnaire contenant comme clés les noms des candidats et comme valeurs la note que chacun a obtenu.

    Cette fonction renvoie la liste contenant les noms des trois meilleurs candidats, par ordre décroissant de leurs notes.

    On garantit que le dictionnaire compte au moins trois couples `(nom: note)` et que les notes sont toutes positives ou nulles et distinctes.

    ```pycon
    >>> scores = {"Candidat 7": 2, "Candidat 2": 38, "Candidat 6": 85, "Candidat 1": 8, "Candidat 3": 17, "Candidat 5": 83,"Candidat 4": 33}
    >>> top_3(scores)
    ['Candidat 6', 'Candidat 5', 'Candidat 2']
    ``` 

    Compléter ci-dessous : 

    {{IDE('../scripts/top_3')}}

    ??? success "Solution"

        ```python
        def top_3(scores):
            # Les noms des gagnants
            premier = None
            deuxieme = None
            troisieme = None
            # Les notes des gagnants
            note_1 = -1
            note_2 = -1
            note_3 = -1
            for nom, note in scores.items():
                if note > note_1:
                    if premier is not None:
                        deuxieme, troisieme = premier, deuxieme
                        note_2, note_3 = note_1, note_2
                    note_1 = note
                    premier = nom
                elif note > note_2:
                    if deuxieme is not None:
                        troisieme = deuxieme
                        note_3 = note_2
                    note_2 = note
                    deuxieme = nom
                elif note > note_3:
                    note_3 = note
                    troisieme = nom
            return [premier, deuxieme, troisieme]
        ```


??? question "Le bon enclos"

    Quels animaux vivent dans l'enclos n°5 ?

    [Le bon enclos](https://codex.forge.apps.education.fr/exercices/bon_enclos/){ .md-button target="_blank" rel="noopener" }


??? question "Valeurs extrêmes"

    Rechercher le maximum et le minimum dans une liste

    [Valeurs extrêmes](https://codex.forge.apps.education.fr/exercices/dict_extremes/){ .md-button target="_blank" rel="noopener" }


??? question "Top likes"

    Quel utilisateur a reçu le plus de *like* ?

    [Top likes](https://codex.forge.apps.education.fr/exercices/top_like/){ .md-button target="_blank" rel="noopener" }


??? question "Dictionnaire des antécédents"

    Sauriez-vous renverser un dictionnaire afin d'obtenir `#!py {valeur: [liste des clés associées]}` ?

    [Dictionnaire des antécédents](https://codex.forge.apps.education.fr/exercices/antecedents/){ .md-button target="_blank" rel="noopener" }


??? question "Dictionnaire de likes"

    On utilise les dictionnaires pour dépouiller des votes.

    [Dictionnaire de likes](https://codex.forge.apps.education.fr/exercices/dictionnaire_aime/){ .md-button target="_blank" rel="noopener" }
