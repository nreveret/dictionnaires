---
author: Mireille Coilhac, Nicolas Revéret
title: Parcours
---

## Clés ✅, valeurs ✅, les deux à la fois ... ✅

### Obtenir toutes les `clés` d'un dictionnaire

Commençons comme nous le faisons avec les listes : `#!py for truc in dico`.

???+ question "Testez par vous même"

    {{IDE('../scripts/parcours_dico')}}

Qu'obtient-on ?

Le dictionnaire peut être représenté dans le tableau ci-dessous :

|       Clé       |  Valeur  |
| :-------------: | :------: |
| `#!py "lapin"`  | `#!py 5` |
| `#!py "vache"`  | `#!py 7` |
| `#!py "cochon"` | `#!py 2` |
| `#!py "cheval"` | `#!py 4` |

On constate donc que la structure `#!py for element in dictionnaire` permet de **parcourir les clés** du dictionnaire.

??? question "Application : montant des courses"

    Gaston va faire ses courses dans une boutique « prix ronds ». Comme il est passionné de `Python`, il a créé le dictionnaire `#!py quantite` pour représenter les quantités achetées et un autre nommé `#!py prix` pour représenter le prix de chaque article.

    Il compte acheter les quantités suivantes :

    | Produit | Quantité     |
    | :------ | :----------- |
    | Beurre  | 2 plaquettes |
    | Farine  | 2 kg         |
    | Sucre   | 1 kg         |


    Il connaît de plus les prix suivants :

    | Produit               | Prix |
    | :-------------------- | :--- |
    | 1kg de farine         | 1 €  |
    | 1 kg de sucre         | 3 €  |
    | 1 L de lait           | 2 €  |
    | 1 plaquette de beurre | 2 €  |

    
    Il construit donc les dictionnaires suivantes :

    ```python
    quantite = {"farine": 2, "beurre": 1, "sucre": 2}
    prix = {"farine": 1, "sucre": 3, "lait": 2, "beurre": 2}
    ```

    Aidez-le à compléter la fonction `#!py montant` qui prend en paramètres ces deux dictionnaires, et renvoie le montant qu'il devra payer pour ses achats. Les valeurs du dictionnaire `#!py prix` sont des entiers.

    {{IDE('../scripts/achats_1')}}

    ??? success "Solution"

        ```python
        def montant(quantite, prix):
            a_payer = 0
            for article in quantite:
                a_payer = a_payer + quantite[article] * prix[article]
            return a_payer
        ```

### Obtenir toutes les `valeurs` d'un dictionnaire

??? question "Les notes d'Alice"

    Alice a créé un dictionnaire avec ses devoirs de NSI : `#!py devoirs_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}`
    
    Aidez-là à compléter la fonction qui renvoie **une** liste des notes qu'elle a obtenues. Cette liste ne sera pas ordonnée.


    {{IDE('../scripts/notes_1')}}

    ??? success "Solution"

        On peut penser à la solution suivante : 

        ```python
        def notes(devoirs):
            liste_notes = []
            for un_devoir in devoirs:
                liste_notes.append(devoirs[un_devoir])
            return liste_notes
        ```

        On aurait aussi pu utiliser une liste en compréhension :

        ```python
        def notes(devoirs):
            return [devoirs[un_devoir] for un_devoir in devoirs]
        ```

### Obtenir tous les couples `(clé: valeur)` d'un dictionnaire

??? question "Les notes d'Alice"

    Alice a toujours sous les yeux un dictionnaire avec ses notes de NSI : `#!py devoirs_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}`

    Aidez-là à compléter la fonction qui renvoie **une** liste de tuples `(devoir, note)` contenant toutes les informations présentes dans le dictionnaire.

    {{IDE('../scripts/notes_2')}}

    ??? success "Solution"

        On peut penser à la solution suivante : 

        ```python
        def bulletin(devoirs):
            liste_notes = []
            for un_devoir in devoirs:
                liste_notes.append((un_devoir, devoirs[un_devoir]))
            return liste_notes
        ```

        On peut utiliser une liste en compréhension

        ```python
        def bulletin(devoirs):
            return [(un_devoir, devoirs[un_devoir]) for un_devoir in devoirs]
        ```

!!! abstract  "À retenir"

    Soit le dictionnaire `#!py dico`.

    * Le parcours d'un dictionnaire se fait sur ses clés en écrivant :

        ```python
        for cle in dico:
            # action à répéter
        ```

    * On peut obtenir une liste `#!py cles` de toutes les clés de ce dictionnaire ainsi :

        ```python
        cles = [cle for cle in dico]
        ```
    
    * On peut obtenir une liste `#!py valeurs` de toutes les valeurs de ce dictionnaire ainsi :

        ```python
        valeurs = [dico[cle] for cle in dico]
        ```

    * On peut obtenir **une** liste des couples `(clé: valeur)` de ce dictionnaire ainsi :

        ```python
        couples = [(cle, dico[cle]) for cle in dico]
        ```

    * Depuis `Python 3.6`, il est garanti que le parcours d'un dictionnaire se fait dans l'ordre d'ajout des couples `(clé: valeur)`. Si l'on n'est pas sûr de la version de `Python` utilisée, on ne peut pas être sûr de l'ordre dans lequel s'effectuent ces différents parcours.


## Vues d'un dictionnaire

!!! danger "Attention"

    Les objets renvoyés par les méthodes `#!py keys`, `#!py values`, `#!py items` sont d'un type particulier que l'on n'étudiera pas ici.
    
    On se contentera donc de les utiliser sans étudier en détails les objets renvoyés.

    On notera toutefois qu'il est toujours possible de se passer de ces méthodes.

Observez directement le résultat de ces différentes méthodes :

???+ question "Testez par vous même"

    {{IDE('../scripts/methodes_1')}}

Comme on peut le voir :

* `#!py for cle in dico.keys()` effectue le parcours des clés de `#!py dico`,
  
* `#!py for valeur in dico.values()` effectue le parcours des valeurs de `#!py dico`,

* `#!py for (cle, valeur in dico.items()` effectue le parcours des tuples (clé: valeur) de `#!py dico`.

Les objets renvoyés par les méthodes `#!py keys`, `#!py values` et `#!py items` sont des objets que l'on appelle des "vues".

```pycon
>>> notes = {"test_1": 14, "test_2": 16, "test_3": 18}
>>> notes.keys()
dict_keys(['test_1', 'test_2', 'test_3'])
```

```pycon
>>> notes.values()
dict_values([14, 16, 18])
```

```pycon
>>> notes.items()
dict_items([('test_1', 14), ('test_2', 16), ('test_3', 18)])
```

Il est toutefois possible de les convertir simplement en listes `Python` en procédant ainsi :

```pycon
>>> list(notes.keys())
['test_1', 'test_2', 'test_3']
```

```pycon
>>> list(notes.values())
[14, 16, 18]
```

```pycon
>>> list(notes.items())
[('test_1', 14), ('test_2', 16), ('test_3', 18)]
```

??? question "`#!py "cheval" in ferme_gaston.keys()` ?"

    On considère le dictionnaire suivant : `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`

    Que renvoie l'expression `#!py "cheval" in ferme_gaston.keys()` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py True`
        - [ ] `#!py False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
      
        - :white_check_mark: `#!py True`
        - :x: ~~`#!py False`~~
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

??? question "Comment parcourir les clés ?"

    On considère le dictionnaire suivant : `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
  
    Quelle instruction permet de parcourir les clés de `#!py ferme_gaston` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py for cle in range(ferme_gaston)`
        - [ ] `#!py for cle in ferme_gaston.keys`
        - [ ] `#!py for cle in ferme_gaston.keys()`
        - [ ] `#!py for cle in keys.ferme_gaston()`
        - [ ] `#!py for cle in ferme_gaston`

    === "Solution"
      
        - :x: ~~`#!py for cle in range(ferme_gaston)`~~
        - :x: ~~`#!py for cle in ferme_gaston.keys`~~ Il ne faut pas oublier les parenthèses.
        - :white_check_mark: `#!py for cle in ferme_gaston.keys()`
        - :x: ~~`#!py for cle in keys.ferme_gaston()`~~
        - :white_check_mark: `#!py for cle in ferme_gaston`

      
??? question "`#!py "vert" in couleurs.values()` ?"

    On considère le dictionnaire suivant : `#!py couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`.

    Que renvoie l'expression `#!py "vert" in couleurs.values()` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py True`
        - [ ] `#!py False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
      
        - :x: ~~`#!py True`~~ 
        - :white_check_mark: `#!py False`
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

??? question "Type de pokemons"

    ```python
    pokemons = {
        "Salamèche" : "Feu",
        "Reptincel": "Feu",
        "Carapuce": "Eau",
        "Chenipan": "Insecte"
    }
    ```
  
    Quelle instruction permet de parcourir les types des pokemons de ce dictionnaire ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py for type_pokemon in range(pokemons)`
        - [ ] `#!py for type_pokemon in pokemons.values()`
        - [ ] `#!py for type_pokemon in pokemons.values`
        - [ ] `#!py for type_pokemon in values.pokemons()`
        - [ ] `#!py for type_pokemon in pokemons`

    === "Solution"
      
        - :x: ~~`#!py for type_pokemon in range(pokemons)`~~
        - :white_check_mark: `#!py for type_pokemon in pokemons.values()`
        - :x: ~~`#!py for type_pokemon in pokemons.values`~~ Il ne faut pas oublier les parenthèses.
        - :x: ~~`#!py for type_pokemon in values.pokemons()`~~
        - :x: ~~`#!py for type_pokemon in pokemons`~~ Cette instruction parcourt les clés


??? question "Utiliser la méthode `#!py values`"
 
    Bob a constitué un dictionnaire de recettes de la façon suivante : les clés sont les noms des gâteaux et les valeurs les listes des ingrédients de chacun d'entre eux.

    Aidez-le à écrire la fonction `#!py nombre_recettes` qui prend en paramètres un tel dictionnaire des recettes, un ingrédient, et qui renvoie le nombre de recettes utilisant cet ingrédient.

    {{IDE('../scripts/parcours_values')}}

    ??? success "Solution"

        En utilisant la méthode `#!py values` on peut faire :

        ```python
        def nombre_recettes(recettes, ingredient):
            total = 0
            for ingredients in recettes.values():
                if ingredient in ingredients:
                    total = total + 1
            return total
        ```

        Il est aussi possible de ne pas l'utiliser :

        ```python
        def nombre_recettes(recettes, ingredient):
            total = 0
            for gateau in recettes:
                if ingredient in recettes[gateau]:
                    total = total + 1
            return total
        ```

??? question "`("dragon", 2) in ferme_gaston.items()` ?"

    `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`

    Que renvoie l'expression `("dragon", 2) in ferme_gaston.items()` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py True`
        - [ ] `#!py False`
        - [ ] Une erreur
        - [ ] Rien

    === "Solution"
      
        - :x: ~~`#!py True`~~
        - :white_check_mark: `#!py False`
        - :x: ~~Une erreur~~
        - :x: ~~Rien~~

??? question "Parcourir les couples `#!py (clé, valeur)`"

    `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
  
    Quelle instruction permet de parcourir les tuples (animal, effectif) de `#!py ferme_gaston` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py for paire in range(ferme_gaston)`
        - [ ] `#!py for paire in items.ferme_gaston`
        - [ ] `#!py for paire in ferme_gaston.items`
        - [ ] `#!py for paire in ferme_gaston.items()`
   
    === "Solution"
      
        - :x: ~~`#!py for paire in range(ferme_gaston)`~~
        - :x: ~~`#!py for paire in items.ferme_gaston`~~
        - :x: ~~`#!py for paire in ferme_gaston.items`~~ Il ne faut pas oublier les parenthèses.
        - :white_check_mark: `#!py for paire in ferme_gaston.items()`



??? question "Utiliser la méthode `#!py items`"

    Alice est étourdie, elle ne se souvient plus à quel devoir elle a obtenu la note "18".

    Compléter la fonction `#!py intitule` ci-dessous qui prend en paramètres un dictionnaire des notes ainsi qu'une note et renvoie le nom d'un devoir auquel cette note est associée.

    Si aucun devoir ne correspond, on renverra la chaîne `#!py "Cette note n'a pas été obtenue"`.

    {{IDE('../scripts/parcours_items')}}

    ??? success "Solution"

        ```python
        def intitule(notes, note) -> str:
            for (nom, resultat) in les_notes.items():
                if resultat == note:
                    return nom
            return "Cette note n'a pas été obtenue"
        ```

        Il est aussi possible de se passer de `#!py items` :

        ```python
        def intitule(notes, note):
            for nom  in les_notes :
                if les_notes[nom] == note:
                    return nom
            return "Cette note n'a pas été obtenue"
        ```

!!! abstract  "À retenir"

    Soit le dictionnaire `#!py dico`.

    * `#!py dico.keys()` permet d'accéder à toutes les clés de `#!py dico`,
  
    * `#!py dico.values()` permet d'accéder à toutes les valeurs de `#!py dico`,

    * `#!py dico.items()` permet*  d'accéder à tous les tuples (clé: valeur) de `#!py dico`.

## QCM's

??? question "Qu'obtient-on ?"

    On considère le dictionnaire`#!py symboles_hexa = {"A": 10, "B": 11, "C": 12, "D": 13, "E":14, "F":15}`

    Que peut afficher `#!py print(list(symboles_hexa.items()))` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `['A', 'B', 'C', 'D', 'E', 'F']`
        - [ ] `[10, 11, 12, 13, 14, 15]`
        - [ ] `[(10, 'A'), (11, 'B') ,(12, 'C'), (13, 'D'), (14, 'E'), (15, 'F')]`
        - [ ] `[('A': 10), ('B': 11), ('C': 12), ('D': 13), ('E': 14), ('F': 15)]`

    === "Solution"
      
        - :x: ~~`['A', 'B', 'C', 'D', 'E', 'F']`~~
        - :x: ~~`[10, 11, 12, 13, 14, 15]`~~
        - :white_check_mark: `[(10,'A'), (11, 'B') ,(12, 'C'), (13, 'D'), (14, 'E'), (15, 'F')]`
        - :x: ~~`[('A': 10), ('B': 11), ('C': 12), ('D': 13), ('E': 14), ('F': 15)]`~~

??? question "Liquidation"


    Que peut-il s'afficher si l'on exécute le script suivant :

    ```python
    stock = {"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}
    for (fruit, nombre) in stock.items():
        if nombre < 5:
            stock[fruit] = 0
    print(stock)
    ```

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `{'pommes': 10, 'kiwis': 0, 'bananes': 6, 'noix': 0}`
        - [ ] `{'kiwis': 0, 'pommes': 10, 'bananes': 6, 'noix': 0}`
        - [ ] `{'0': 2, 'pommes': 10, 'bananes': 6, '0': 4}`
        - [ ] `{'kiwis': 2, 'pommes': 10, 'bananes': 6, 'noix': 4}`

    === "Solution"
      
        - :white_check_mark: `{'pommes': 10, 'kiwis': 0, 'bananes': 6, 'noix': 0}` Il n'y a pas d'ordre pour écrire un dictionnaire.
        - :white_check_mark: `{'kiwis': 0, 'pommes': 10, 'bananes': 6, 'noix': 0}`
        - :x: ~~`{'0': 2, 'pommes': 10, 'bananes': 6, '0': 4}`~~
        - :x: ~~`{'kiwis': 2, 'pommes': 10, 'bananes': 6, 'noix': 4}`~~ Il faut modifier les valeurs strictement inférieures à 5 en 0.

??? question "Quelle couleur ?"

    `#!py couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`

    Que renvoie l'expression `#!py couleurs[0]` ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `'white'`
        - [ ] `'blanc'`
        - [ ] Une erreur
      

    === "Solution"
      
        - :x: ~~`"white"`~~
        - :x: ~~`"blanc"`~~
        - :white_check_mark: Une erreur. Un dictionnaire ne comporte pas d'indices. Il faut utiliser une clé.
       

??? question "Coloriage"

    On considère le dictionnaire `#!py couleurs = {"white": "blanc", "black": "noir", "red": "rouge"}`

    Quelle instruction faut-il écrire pour modifier ce dictionnaire et obtenir `#!py couleurs = {"white": "blanc", "black": "noir", "red": "rouge, "green": "vert"}`.

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py couleurs["green"] = "vert"`
        - [ ] `#!py couleurs.append("green", "vert")`
        - [ ] Ce n'est pas possible
   
    === "Solution"
      
        - :white_check_mark: `#!py couleurs["green"] = "vert"`
        - :x: ~~`#!py couleurs.append("green", "vert")`~~
        - :x: ~~Ce n'est pas possible~~

??? question "Le fruit le plus fort !"

    On considère le dictionnaire `#!py stock = {"kiwis": 2, "pommes": 10, "bananes": 6, "noix": 4}`.

    Quelle est l'instruction qui permet d'afficher le plus grand nombre de fruits d'une variété ?

    === "Cocher la ou les affirmations correctes"
      
        - [ ] `#!py print(max(stock))`
        - [ ] `#!py print(max(stock.values()))`
        - [ ] `#!py print(max(stock.values))`
        - [ ] `#!py print(max([stock[fruit] for fruit in stock]))`
   
    === "Solution"
      
        - :x: ~~`#!py print(max(stock))`~~ 
        - :white_check_mark: `#!py print(max(stock.values()))`
        - :x: ~~`#!py print(max(stock.values))`~~ Ne pas oublier les parenthèses
        - :white_check_mark: `#!py print(max([stock[fruit] for fruit in stock]))`
  
