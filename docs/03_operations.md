---
author: M. Boehm, C. Poulmaire, P. Remy, M. Coilhac, N. Revéret
title: Opérations sur les dictionnaires
---

## Opérations sur les dictionnaires

### Appartenance

`Python` permet de tester facilement si une clé est présente dans un dictionnaire en faisant `#!py clé in dictionnaire`.

???+ question "Testez par vous même"

    {{IDE('../scripts/in_1')}}
    

    
### Accéder à une valeur

On récupère la valeur associée à une clé en faisant `#!py dico[clé]`.

???+ question "Testez par vous même"

    {{IDE('../scripts/acces_valeur_1')}}


### Modifier une valeur

On peut modifier la valeur associée à une clé en faisant une simple affectation : `#!py dico[clé] = nouvelle_valeur`.

???+ question "Testez par vous même"

    {{IDE('../scripts/modif_valeur_1')}}


### Supprimer un couple `(clé: valeur)`

Il existe deux techniques permettant de supprimer une couple `(clé: valeur)`.

* En utilisant `del` :

    ???+ question "Testez par vous-même"

        {{IDE('../scripts/suppr_cle_valeur_1')}}

* En utilisant `pop` qui revoie au passage la valeur associée :

    ???+ question "Testez par vous-même"

        {{IDE('../scripts/suppr_cle_valeur_2')}}


### Ajouter un couple `(clé: valeur)`

???+ question "Ajoutez des dragons"

    Vous savez déjà rajouter des éléments dans un dictionnaires, puisque vous l'avez fait pour créer un dictionnaire à  partir d'un vide.
    
    Compléter le script suivant pour ajouter le couple `("dragon": 2)`.

    {{IDE('../scripts/ajout_cle_valeur_1')}}

    ??? success "Solution"

        ```python
        ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}
        ferme_gaston["dragon"] = 2
        ```



### Longueur d'un dictionnaire

Pour savoir combien un dictionnaire contient de couples `(clés, valeur)`, on fait ▮▮▮.

???+ question "~~Testez~~ Trouvez par vous-même !"

    Pensez aux listes !

    {{IDE('../scripts/longueur')}}

    ??? success "Solution"

        ```python
        ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}
        longueur = len(ferme_gaston)
        ```

## Bilan

!!! abstract  "À retenir"

    Soit `#!py mon_dico` un dictionnaire. Soit `#!py valeur` la valeur associée à `#!py cle` :

    * `#!py cle in mon_dico` renvoie `#!py True` si la clé `cle` existe dans `mon_dico` et `#!py False` sinon.  

    * `#!py mon_dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire.
    
        **Attention**, si la clé n'est pas présente dans le dictionnaire, cette instruction génère une erreur `#!py KeyError` !

    * Si la clé existe déjà `#!py mon_dico[cle] = valeur` modifie la valeur associée.
    
    * Si la clé n'existe pas, `#!py mon_dico[cle] = valeur` ajoute la paire `#!py (cle, valeur)`.

    * `#!py del mon_dico[cle]` supprime le couple `(cle, valeur)` de `#!py mon_dico`.
  
    * `#!py mon_dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `#!py mon_dico` et renvoie la valeur associée.
    
    * `#!py len(mon_dico)` renvoie le nombre de couples `(clé: valeur)` du dictionnaire.


## QCM's

??? question "Accéder à une valeur"

    On considère le dictionnaire `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`.

    On veut accéder au nombre de lapins. Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py ferme_gaston[0]`
        - [ ] `#!py ferme_gaston["lapin"]`
        - [ ] `#!py ferme_gaston("lapin")`
        - [ ] `#!py ferme_gaston(lapin)`

    === "Solution"
        
        - :x: ~~`#!py ferme_gaston[0]`~~ On accède à une valeur grâce à une clé
        - :white_check_mark: `#!py ferme_gaston["lapin"]`
        - :x: ~~`#!py ferme_gaston("lapin")`~~ Il faut des crochets, non des parenthèses
        - :x: ~~`#!py ferme_gaston(lapin)`~~ "lapin" est une chaîne de caractères. Ne pas oublier les guillemets !

??? question "Modifier une valeur"

    On considère le dictionnaire `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    
    Gaston a acheté 3 lapins. Il faut modifier le dictionnaire.  
    
    Quelle commande peut-on saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py ferme_gaston[0] = 8`
        - [ ] `#!py ("lapin" : 5) = ("lapin" : 8)`
        - [ ] `#!py ferme_gaston("lapin") = 8`
        - [ ] `#!py ferme_gaston["lapin"] = 8`
        - [ ] `#!py ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

    === "Solution"
        
        - :x: ~~`#!py ferme_gaston[0] = 8`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`#!py ("lapin" : 5) = ("lapin" : 8)`~~
        - :x: ~~`ferme_gaston("lapin") = 8`~~ Il faut des crochets, non des parenthèses
        - :white_check_mark: `#!py ferme_gaston["lapin"] = 8` juste mais préférer la réponse ci-dessous
        - :white_check_mark: `#!py ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

??? question "Ajouter un couple `(clé: valeur)`"

    On considère le dictionnaire `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    
    On veut ajouter la paire `#!py "dragon : 2"` Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py ferme_gaston["dragon": 2]`
        - [ ] `#!py ferme_gaston["dragon"] = 2`
        - [ ] `#!py 2 = ferme_gaston["dragon"]`
        - [ ] `#!py ferme_gaston["dragon", 2]`
        - [ ] `#!py ferme_gaston("dragon", 2)`

    === "Solution"
        
        - :x: ~~`#!py ferme_gaston["dragon": 2]`~~
        - :white_check_mark: `#!py ferme_gaston["dragon"] = 2`
        - :x: ~~`#!py 2 = ferme_gaston["dragon"]`~~
        - :x: ~~`#!py ferme_gaston["dragon", 2]`~~
        - :x: ~~`#!py ferme_gaston("dragon", 2)`~~

??? question "Supprimer une valeur"

    On considère le dictionnaire `#!py ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    
    On veut supprimer la paire `#!py "dragon": 2`.
    
    Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py del ferme_gaston("dragon")`
        - [ ] `#!py del ferme_gaston["dragon"]`
        - [ ] `#!py ferme_gaston.pop("dragon")`
        - [ ] `#!py ferme_gaston.pop["dragon"]`

    === "Solution"
        
        - :x: `#!py del ferme_gaston("dragon")`
        - :white_check_mark: `#!py del ferme_gaston["dragon"]`
        - :white_check_mark: `#!py ferme_gaston.pop("dragon")`
        - :x: `#!py ferme_gaston.pop["dragon"]`

??? question "Longueur d'un dictionnaire"

    On considère le dictionnaire `#!py ferme_gaston = {"lapin": 6, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    
    Que vaut l'expression `#!py len(ferme_gaston)` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py 2`
        - [ ] `#!py 5`
        - [ ] `#!py 10`
        - [ ] `#!py None`
        - [ ] Elle renvoie un message d'erreur

    === "Solution"
        
        - :x: `#!py 2`
        - :white_check_mark: `#!py 5`
        - :x: `#!py 10`
        - :x: `#!py None`
        - :x: Elle renvoie un message d'erreur

## Exercices


??? question "Gaston compte ses animaux"

    Compléter la fonction `nombre_animaux` qui prend en paramètres `animal` de type `#!py str` et `ferme` de type `#!py dict`.

    Elle renvoie la valeur associée à `animal` si  `animal` est une clé de `ferme`, et `#!py 0` sinon.
       
    {{IDE('../scripts/appartient_sujet')}}

    ??? success "Solution"

        ```python
        def nombre_animaux(animal, ferme):
            if animal in ferme:
                return ferme[animal]
            else:
                return 0
        ```

??? question "Création d'un dictionnaire"

    Saviez-vous qu'il était possible de représenter un automate à l'aide d'un dictionnaire ?

    [Automate en dictionnaire](https://codex.forge.apps.education.fr/exercices/automate_0/){ .md-button target="_blank" rel="noopener" }


??? question "Des ~~chiffres~~ règles et des lettres"

    On utilise le dictionnaire pour modifier des chaînes de caractères... Logique ?

    [Des règles et des lettres](https://codex.forge.apps.education.fr/exercices/lsystem/){ .md-button target="_blank" rel="noopener" }

??? question "Du romain au décimal"

    Pouvez-vous convertir automatiquement les chiffres romains en base 10 ?

    [Romain en décimal](https://codex.forge.apps.education.fr/exercices/romain_decimal/){ .md-button target="_blank" rel="noopener" }
