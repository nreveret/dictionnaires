def intitule(les_notes, note) -> str:
    for (nom, resultat) in les_notes.items():
        if resultat == note:
            return nom
    return "Cette note n'a pas été obtenue"


notes_Alice = {"test_1": 14, "test_2": 16, "test_3": 18}
assert intitule(notes_Alice, 18) == "test_3"
assert intitule(notes_Alice, 20) == "Cette note n'a pas été obtenue"
