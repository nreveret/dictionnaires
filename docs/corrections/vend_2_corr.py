def vend(ferme):
    a_supprimer = []
    for cle in ferme:
        ferme[cle] = ferme[cle] - 1
        if ferme[cle] == 0:
            a_supprimer.append(cle)
    for cle in a_supprimer :
        del ferme[cle]

ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}
vend(ferme_gaston)
assert ferme_gaston == {"lapin": 4, "vache": 6, "cheval": 3}

