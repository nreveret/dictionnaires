---
author: Mireille Coilhac, Nicolas Revéret
title: Création de dictionnaires
---

##  `dico = {🐉: 2, 🐷: 5, 🐂: 40}`


Un dictionnaire est un ensemble **non ordonné** de couples (clé: valeur) notés `#!py clé: valeur`.

Chaque **clé** est présente de façon **unique** dans le dictionnaire.

Un dictionnaire s'écrit avec des **accolades**, les différents couples sont séparés par des **virgules** : `#!py {clé: valeur, autre_clé: autre_valeur, ...}`.

Afin de pouvoir être hachées, les clés doivent être de types **immuables** : elles ne peuvent pas être modifiées.

On peut choisir comme clé des nombres, des chaînes de caractères, des tuples d'éléments de types immuables,  mais **jamais des listes ni des dictionnaires**.

En revanche, une valeur peut être de n'importe quel type : nombre, liste, chaîne de caractères, tuple, ou même dictionnaire.

```python
ferme_gaston = {
    "lapin": 5,
    "vache": 7,
    "cochon": 2,
    "cheval": 4
}
amis_gaston = {
    612457899 : "Gustave",
    712345678 : "Alice",
    998876554 : "Bob"
}  # (Python interdit de faire débuter un entier par 0...)
```


???+ question "Clés valides"

    Parmi les propositions suivantes, laquelle ou lesquelles peuvent être utilisée(s) comme clé d'un dictionnaire ?

    === "Cocher la ou les bonnes réponses"
        
        - [ ] `#!py "toto"`
        - [ ] `#!py ["pomme", "Golden"]`
        - [ ] `#!py 5`
        - [ ] `#!py ("pain", "baguette")`

    === "Solution"
        
        - :white_check_mark: Une chaîne de caractères est immuable
        - :x: Une liste **n'est pas immuable**
        - :white_check_mark: Un entier est immuable
        - :white_check_mark: Un tuple de chaînes de caractères est immuable


???+ question "Qui fait quoi ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] On peut choisir `#!py "Kouign amann"` comme clé d'un dictionnaire
        - [ ] On peut choisir `#!py ["farine", "beurre", "sucre"]` comme clé d'un dictionnaire 
        - [ ] On peut choisir `#!py "Kouign amann"` comme valeur d'un dictionnaire 
        - [ ] On peut choisir `#!py ["farine", "beurre", "sucre"]` comme valeur d'un dictionnaire 

    === "Solution"
        
        - :white_check_mark: `"Kouign amann"` est une chaîne de caractères immuable
        - :x: Une liste **n'est pas immuable** : on ne peut pas l'utiliser comme clé
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur


!!! warning "Le mot *dictionnaire*"

    Dans la vie courante, nous cherchons le sens des mots dans des *dictionnaires*. Ces mots peuvent être considérés comme des « clés ». Les « valeurs » associées à ces clés sont les définitions fournies.
    
    Dans ces dictionnaires les clés sont rangées par ordre lexicographique pour permettre une recherche relativement rapide.
    
    Dans un dictionnaire `Python`, nous avons vu que l'ordre dans lequel sont écrits les couples « clé: valeur » n'a pas d'importance. C'est un processus décrit dans les compléments (tables de hachage) qui permet une recherche encore plus rapide (indépendante du nombre d'éléments).
    

## Création d'un dictionnaire

Il existe plusieurs façon de créer un dictionnaire :

* Création d'un dictionnaire vide :

    ```python
    # Avec des accolades
    dico_vide = {}
    # En étant explicite : on crée un dict
    autre_dico_vide = dict()
    ```

* Écriture directe du dictionnaire dans le code

    On écrit directement les couples dans le code `#!py dictionnaire = {cle: valeur , autre_cle: autre_valeur, …}`.

    ```python
    figurines = {"licorne": 2, "yeti": 1, "Pégase": 1, "dragon": 3}
    ```

* En complétant un dictionnaire vide

    Une façon très pratique de créer un dictionnaire, est de partir d'un dictionnaire vide. Il suffit ensuite d'insérer des couples **(clé : valeur)** en utilisant les clés :

    ???+ question "Ajouter des couples"

        Compléter le script ci-dessous afin d'ajouter des couples dans le `panier` :

        {{IDE('../scripts/panier_1')}}

Il existe d'autres méthodes pour créer des dictionnaires. On pourrait par exemple utiliser un dictionnaire en compréhension. Par exemple, `#!py lettre = {k: ord(97 + k) for k in range(26)}` permet de créer le dictionnaire `#!py {0: 'a', 1: 'b', ..., 25: 'z'}`.

## Application : Bob fait ses courses !

Vous allez aider Bob a constituer le dictionnaire de ses achats. 

Vous devez compléter la fonction `#!py courses` qui prend en paramètres deux listes `#!py produits` et `#!py quantites`.

* `#!py produits` est une liste de chaîne de caractères ;

* `#!py quantites` est une liste d'entiers ;

* `#!py produits` et `#!py quantites` sont de même longueur.


Par exemple `#!py courses(["farine", "beurre", "œufs"], [1, 2, 12])` doit renvoyer `#!py {'farine': 1, 'beurre': 2, 'œufs': 12}` (sans tenir compte de l'ordre des couples).

???+ question "Fonction `#!py courses`"

    Compléter le script ci-dessous :

    {{IDE("../scripts/bob_1")}}


    ??? success "Solution"

        ```python
        def courses(produits, quantites):
            panier = {}
            for i in range(len(produits)):
                panier[produits[i]] = quantites[i]
            return panier
        ```

!!! note "Et dans les langages autres que Python ?"

    Les dictionnaires sont également présents dans d'autres langages sous le nom de « mémoires associatives » ou de « tableaux associatifs ».

    Voici un exemple en `Javascript` :

    ```javascript
    let panier = new Map()

    panier.set("farine", 1)
    panier.set("beurre", 2)
    panier.set("œufs", 12)

    console.log(panier.get("œufs")) // affiche dans la console le nombre d'œufs à acheter
    ```
