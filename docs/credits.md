---
author: Mireille Coilhac, Nicolas Revéret
title: 👍🏽 Crédits
---

* Ce site a été réalisé par Mireille Coilhac, avec l'aide de Charles Poulmaire, Jean-Louis Thirot et des professeurs du groupe e-nsi. Il a été relu et repris par Nicolas Revéret.

* L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

* Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

* Le logo [Grammar a été créé par Freepik - Flaticon](https://www.flaticon.com/free-icons/grammar).