# Les dictionnaires

Ce site a pour but de faire découvrir les dictionnaires `Python` aux élèves de Première NSI.

On y trouve différentes sections :

- [🏁 Premiers pas](01_quesako.md) : une première approche ;
- [📝 Créations](02_def_dico.md) : comment créer un dictionnaire ;
- [🧰 Opérations](03_operations.md) : les opérations fondamentales ;
- [🧭 Parcours](04_parcours.md) : comment parcourir un dictionnaire ;
- [🏁 Bilan](05_bilan.md) : le... bilan !
- [👷🏽 Exercices](06_exercices.md) : on applique tout ce qui précède ;
- [🐦 Compléments](07_complements.md) : quelques approfondissements.

Les crédits du site sont [ici](credits.md). Un grand merci à [Mireille Coilhac](https://forge.aeif.fr/mcoilhac) qui a rédigé l'ensemble des pages. Ce site est une adaptation de son travail.